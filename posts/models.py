from django.db import models
from django.contrib.auth.models import User
from projects.models import Project

# Create your models here.


class Post(models.Model):
    message = models.TextField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(
        User,
        related_name="posts",
        on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        Project,
        related_name="posts",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ["-date"]
