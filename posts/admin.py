from django.contrib import admin
from posts.models import Post

# Register your models here.


@admin.register(Post)
class PostConfig(admin.ModelAdmin):
    list_display = ("project", "user", "date", "id")
