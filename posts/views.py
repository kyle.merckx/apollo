from django.shortcuts import render, redirect
from posts.models import Post
from projects.models import Project
from posts.forms import PostForm

# Create your views here.


def create_post(request, id):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid:
            post = form.save(False)
            post.user = request.user
            post.project = Project.objects.get(id=id)
            post.save()
            return redirect("show_project", id=id)

    else:
        form = PostForm()

    context = {
        "form": form
    }

    return render(request, "posts/create.html", context)
