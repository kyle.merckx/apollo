from django.urls import path
from posts.views import create_post


urlpatterns = [
    path("create/<int:id>/", create_post, name="create_post")
]
