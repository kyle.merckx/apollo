from django.contrib import admin
from audiofiles.models import AudioFile
# Register your models here.


@admin.register(AudioFile)
class AudioFileAdmin(admin.ModelAdmin):
    list_display = ("name", "notes", "file", "project", "user", "date")
