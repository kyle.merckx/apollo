from django.shortcuts import render, redirect
from audiofiles.models import AudioFile
from audiofiles.forms import AudioFileForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def list_audiofiles(request):
    files = AudioFile.objects.filter(user=request.user)

    context = {
        "files": files
    }

    return render(request, "audiofiles/list.html", context)

@login_required
def upload_audiofile(request):
    if request.method == "POST":
        form = AudioFileForm(request.POST, request.FILES)
        if form.is_valid():
            audiofile = form.save(False)
            audiofile.user = request.user
            audiofile.save()
            return redirect("list_audiofiles")

    else:
        form = AudioFileForm()

    context = {
        "form": form
    }

    return render(request, "audiofiles/upload.html", context)
