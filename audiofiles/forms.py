from django import forms
from audiofiles.models import AudioFile


class AudioFileForm(forms.ModelForm):
    class Meta:
        model = AudioFile
        fields = ("name", "notes", "file", "project")
