from django.urls import path
from audiofiles.views import list_audiofiles, upload_audiofile


urlpatterns = [
    path("audiofiles/", list_audiofiles, name="list_audiofiles"),
    path("upload/", upload_audiofile, name="upload_audiofile")
]
