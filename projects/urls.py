from django.urls import path
from projects.views import list_projects, list_my_projects, show_project, create_project


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("mine", list_my_projects, name="list_my_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("<int:id>/post", show_project, name="show_project1"), #this needs work
    path("create/", create_project, name="create_project"),
]
