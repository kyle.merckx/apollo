from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    is_private = models.BooleanField(default=False, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, null=True)

    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-date"]
