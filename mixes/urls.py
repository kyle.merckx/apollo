from django.urls import path
from mixes.views import upload_mix


urlpatterns = [
    path("upload/", upload_mix, name="upload_mix")
]
