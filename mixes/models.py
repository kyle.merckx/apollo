from django.db import models
from projects.models import Project
from django.contrib.auth.models import User

# Create your models here.


class Mix(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(null=True, upload_to="mixes/audio")
    notes = models.TextField(max_length=500)
    date = models.DateTimeField(auto_now_add=True, null=True)

    project = models.ForeignKey(
        Project,
        related_name="mixes",
        on_delete=models.PROTECT
    )
    user = models.ForeignKey(
        User, related_name="mixes", on_delete=models.CASCADE
        )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-date"]
