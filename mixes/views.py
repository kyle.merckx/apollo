from django.shortcuts import render, redirect
from mixes.models import Mix
from mixes.forms import MixForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def upload_mix(request):
    if request.method == "POST":
        form = MixForm(request.POST, request.FILES)
        if form.is_valid():
            mix = form.save(False)
            mix.user = request.user
            mix.save()
            return redirect("list_audiofiles")

    else:
        form = MixForm()

    context = {
        "form": form
    }

    return render(request, "mixes/upload.html", context)
