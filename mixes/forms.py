from django import forms
from mixes.models import Mix


class MixForm(forms.ModelForm):
    class Meta:
        model = Mix
        fields = ("name", "notes", "file", "project")
