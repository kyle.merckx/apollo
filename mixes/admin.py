from django.contrib import admin
from mixes.models import Mix

# Register your models here.


@admin.register(Mix)
class MixConfig(admin.ModelAdmin):
    list_display = ("name", "notes", "user", "file", "date")
